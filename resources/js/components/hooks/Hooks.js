import React, { useState, useEffect, useContext, createContext, useReducer } from 'react'

/* useState */
export const Counter = () => {
    const array = ['dog', 'cat', 'sheep']
    const [first, second] = array
    console.log(first, second)
    const [value, setValue] = useState(0)
    return (
        <div>
            <p>
                현재 카운터 값은 <b>{value}</b> 입니다.
            </p>
            <button onClick={() => setValue(value + 1)}>+1</button>
            <button onClick={() => setValue(value - 1)}>-1</button>
        </div>
    )
}

/* useEffect */
export const Info = () => {
    const [name, setName] = useState('')
    const [nickname, setNickname] = useState('')

    useEffect(() => {   // 실행과 업데이트 시에
        console.log('effect')
        console.log({
            name,
            nickname
        })
        return () => { // update 발생시 직전에 실행 , 지워지기 직전에 실행
            console.log('cleanup')
            console.log(nickname)
        }
    },[name])   // [] 배열 추가시 실행시에만 A 실행하고 업데이트시 A 미작동 X ,
                // [object] 추가시 해당 객체 업데이트만 A 실행


    const [counter, setCounter] = useState(0)
    const onChangeName = e => {
        setName(e.target.value)
        setCounter(counter+1)
    }
    const onChangeNickName = e => {
        setNickname(e.target.value)
        setCounter(counter+1)
    }


    console.log(counter)
    return (
        <div>
            <div>
                <input value={name} onChange={onChangeName}/>
                <input value={nickname} onChange={onChangeNickName}/>
            </div>
            <div>
                <p><b>이름:</b> {name}</p>
                <p><b>닉네임:</b> {nickname}</p>
            </div>
        </div>
    )
}

/* useContext */
const ThemeContext = createContext('black')
export const ContextSample = () => {
    const theme = useContext(ThemeContext)
    const style = {
        width: '24px',
        height: '24px',
        background: theme
    }
    return (
        <div style={style}>

        </div>
    )
}

/* useReducer */
export const ReducerCounter = () => {
    return (
        <div>
            reducer
        </div>
    )
}


